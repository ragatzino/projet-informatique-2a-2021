<!-- .slide: data-background-image="images/project.png" data-background-size="1200px" class="chapter" -->

## Déroulement du projet

%%%


### Organisation classique

<img src="images/project-progress.png"/>

%%%

<!-- .slide: data-background-image="images/project.png" data-background-size="600px" class="slide" -->

### Les deadlines et les livrables

- 17 octobre : Remise du dossier d'architecture
- 26 novembre 20h : rapport final et code définitif
- 7 décembre : soutenance

%%%

<!-- .slide: data-background-image="images/project.png" data-background-size="600px" class="slide" -->

### Proposition de méthodes de travail

Fonctions "faciles" 
    - pair programming : 2 sur un poste (de préférence de niveau différent)

Fonctions "complexes" et initialisation du projet 
    - mob programming : tous sur un poste

%%%

<!-- .slide: data-background-image="images/project.png" data-background-size="600px" class="slide" -->


### Petit tips

- Passez du temps sur l'étude préalable et la conception générale
- Cernez vos cas d'utilisations, les acteurs etc (diagramme CU)
- L'autre diagramme important est celui de classes, il doit guider vos développements
- Priorisez les développements par la valeur
- Développez un ensemble de petits modules simples plutôt qu'une énorme main

%%%

<!-- .slide: data-background-image="images/project.png" data-background-size="600px" class="slide" -->

### Liens utiles
<ul>
<li> 
<a href="https://loufranco.com/wp-content/uploads/2012/11/cheatsheet.pdf">
 Cheat sheet UML 
 </a>
 </li>

<li> 
<a href="https://www.lucidchart.com/pages/fr/diagramme-de-classes-uml">
 Aide diagramme de classes 
 </a>
 </li>


</ul>


