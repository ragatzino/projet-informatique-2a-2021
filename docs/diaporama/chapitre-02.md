<!-- .slide: data-background-image="images/Roll20-Logo.png" data-background-size="600px" class="chapter" -->

## Rappel du besoin

%%%

<!-- .slide: data-background-image="images/Roll20-Logo.png" data-background-size="200px" class="slide"   -->

### Un petit rappel sur le jeu de rôle ?

un jeu de rôle est la réunion de plusieurs composantes :
- des participants à la partie (joueurs) 
- pouvant communiquer les uns avec les autres 
- interagissant dans un espace partagé imaginaire 
- interprétant un (ou plusieurs) rôles (quoique cette composante puisse parfois être mise de côté sous certaines conditions) 
- avec au moins un mécanisme pour arbitrer les situations incertaines (aléatoire, Maitre du jeu,..)

%%%

<!-- .slide: data-background-image="images/Roll20-Logo.png" data-background-size="200px"  class="slide" -->

### Fonctionnalités de base attendues

- Créer une campagne de jeu de rôle
- Créer une fiche de personnage
- Lancer des dés
- Stocker l'état de la partie en bdd et pouvoir l'exporter
- Construction d'une api webservice et d'un client dédié a l'application.

%%%

<!-- .slide: data-background-image="images/Roll20-Logo.png" data-background-size="200px"  class="slide" -->

### Fonctionnalités avancées

Au moins une est exigée 

- Implémenter un outil de création de personnages.
- Import d'une sauvegarde depuis le début.
- Gérer automatiquement les gains et pertes de vie.
- Mettre en place une authentification.
- Gérer une carte

%%%
<!-- .slide: data-background-image="images/Roll20-Logo.png" data-background-size="200px"  class="slide" -->

### C'est votre application

Soyez originaux et n'hésitez pas a me contacter si vous voulez vous aventurer hors des sentiers battus ! :)

